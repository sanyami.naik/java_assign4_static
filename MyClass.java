package Assignments.four;


public class MyClass {
    private static int x = 10;

    static {
        x++;
    }

    static {
        ++x;
    }

    {//object initialiazation block gets called on object creation always and is created outside the constructor method or block
        x--;
    }


    public static void main(String[] args) {
        MyClass obj = new MyClass();
        MyClass obj2 = new MyClass();
        MyClass obj3 = new MyClass();
        System.out.println(x);
    }
}