package Assignments.four;

class LoadDemo
{

    static void method()
    {
        System.out.println("method without parameters");
    }

    static void method(int a)
    {
        System.out.println("method with parameters");
    }

}
public class OverloadStatic {
    public static void main(String[] args) {
        LoadDemo.method();
        LoadDemo.method(2);

    }
}
