package Assignments.four;


class Parent
{
    static void method(){
        System.out.println("In parent class method");
    }

}
class Child extends Parent
{
    static void method()                         //this is the static meth of child class individually
                                                 //This is not called as overriding
    {
        System.out.println("In child class method");
    }
}

public class OverrideStatic {
    public static void main(String[] args) {

        Parent.method();
        Child.method();
    }

}
