package Assignments.four;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class UseStatic {

    static int a=100;
    static
    {
        System.out.println("In the static block ");
    }
    static void method()
    {
        System.out.println("In the static method and "+a);
    }

    static class Sclass
    {
         static void smethod()
        {
            System.out.println("In the static method of inner static class!! value of a is " + a);
            Sclass s=new Sclass();
            s.imethod();
        }

        void imethod()
        {
            System.out.println("In the instance method of inner static class!! value of a is " + a);
        }

    }

    public static void main(String[] args) {

        UseStatic.method();
        UseStatic.Sclass.smethod();

    }
}
